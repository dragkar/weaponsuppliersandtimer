package WeaponSuppliers;


public class Supplier {


    private GunWarehouse gunWarehouse = new GunWarehouse();
    private KnifeWarehouse knifeWarehouse = new KnifeWarehouse();
    private BFGWarehouse bfgWarehouse = new BFGWarehouse();

    public void supply(int guns, int knifes, int bfgs) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < guns; i++) {
                    synchronized (GunWarehouse.class) {
                        gunWarehouse.add();
                    }
                }
            }
        }).start();


        new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < knifes; i++) {
                    synchronized (knifeWarehouse) {
                        knifeWarehouse.add();
                    }
                }
            }
        }).start();


        new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < bfgs; i++) {
                    synchronized (bfgWarehouse) {
                        bfgWarehouse.add();
                    }
                }
            }
        }).start();
    }
}
